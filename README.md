# Giuseppe's Verivox code challenge

Hi! My name is **Giuseppe** and this is my code challenge.

## How to run

Once you've checked out the repo, just run `yarn install` and then `yarn start`.
Now you can head up to [http://localhost:3000/](http://localhost:3000/) and see the frontend. 
If you want to query the backend directly instead, you can try one of these urls: 

* [http://localhost:3001/products](http://localhost:3001/products) - shows a list of all "products"
* [http://localhost:3001/products/base000](http://localhost:3001/products/base000) - the details of a single product
* [http://localhost:3001/offers/usage/3000](http://localhost:3001/offers/usage/3000) - given the usage, it returns the offers


## What I've used

Both frontend and backend are written in **typescript**. 

### Backend
Here I've used node.js with a little library ([overnight.js](https://github.com/seanpmaxwell/overnight)) that helped me reducing the boilerplate.
I've also modelled the definitions using a swagger file, I did it by looking at the current structure I was able to see in the responses of the live website.

> If you are into swagger, then it might be worth having a look at `/docs/swagger/tariff-api.json`.

Those definitions are generated every time you run the application (of course this is an overkill, I left it in just to show off a bit :-D).

### Frontend
I didn't want to use React. Don't get me wrong, I do like React but this time I wanted to use something else and I've tried with [Preact](https://preactjs.com/). 
The frontend code is sloppy (having more time I would have separated the elements in single components, used scss, use the defintions from swagger and so on...).

## Contacts

Any questions, you can find me here: hello@giuseppe.dev
