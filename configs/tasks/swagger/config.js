const cwd = process.cwd();

module.exports = {
	projectRoot: cwd,
	fapiFileList: `${cwd}/docs/swagger/*.*`, // the swagger sources
	outputPath: `${cwd}/src/server/shared/models/swagger`, // where to deliver the generated models
	swaggerGeneratorBinary: `${__dirname}/swagger-codegen-cli.jar`
};
