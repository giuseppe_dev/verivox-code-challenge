#!/usr/bin/env node

const execa = require('execa');
const path = require('path');
const util = require('util');
const glob = util.promisify(require('glob'));
const Listr = require('listr');
const config = require('./config');

(async () => {
	const tasks = new Listr();
	const files = await glob(config.fapiFileList, { realpath: true });

	files.forEach(async file => {
		const fileName = path.basename(file, path.extname(file));
		const outputDir = config.outputPath;

		const cmd = `java -jar ${
			config.swaggerGeneratorBinary
		} generate -Dmodels -i ${file} -l typescript-angular -o ${outputDir}`;

		tasks.add({
			title: 'Generate models from ' + path.basename(file),
			task: (ctx, task) =>
				execa.stdout(cmd, { shell: true }).catch(() => {
					task.skip('Swagger file corrupt.');
				})
		});
	});

	tasks.run().catch(err => {
		console.error(err);
	});
})();
