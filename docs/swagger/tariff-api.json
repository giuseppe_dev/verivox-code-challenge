{
	"swagger": "2.0",
	"info": {
		"description": "A simplified tariff comparison API.",
		"version": "1.0.0",
		"title": "Verivox - Giuseppe's code challenge",
		"contact": {
			"email": "hello@giuseppe.dev"
		}
	},
	"basePath": "/v0",
	"tags": [
		{
			"name": "tariff api",
			"description": "Minimal functionality to retrieve the best offer out of provided consumption data"
		}
	],
	"schemes": ["https"],
	"paths": {
		"/products": {
			"get": {
				"tags": ["products"],
				"summary": "Retrieve the full list of available products",
				"description": "Returns all the available products",
				"operationId": "getAllProducts",
				"parameters": [
					{
						"in": "query",
						"name": "usage",
						"type": "number",
						"format": "integer"
					}
				],
				"produces": ["application/json"],
				"responses": {
					"200": {
						"description": "successful operation",
						"schema": {
							"type": "array",
							"items": {
								"$ref": "#/definitions/Product"
							}
						}
					},
					"400": {
						"description": "Invalid request"
					},
					"404": {
						"description": "No products found"
					}
				}
			}
		},
		"/products/{id}": {
			"get": {
				"tags": ["products"],
				"summary": "Retrieve a single product details",
				"description": "Returns a specific product (not really needed in this exercise, just put it here out of habit)",
				"operationId": "getProduct",
				"parameters": [
					{
						"in": "path",
						"name": "id",
						"type": "string",
						"required": true,
						"description": "The product identifier"
					}
				],
				"produces": ["application/json"],
				"responses": {
					"200": {
						"description": "successful operation",
						"schema": {
							"$ref": "#/definitions/Product"
						}
					},
					"400": {
						"description": "Invalid request"
					},
					"404": {
						"description": "No product found"
					}
				}
			}
		}
	},
	"definitions": {
		"Product": {
			"type": "object",
			"description": "The product container, this model contains the product metadata and the connected tariff",
			"properties": {
				"id": {
					"type": "string",
					"example": "id123",
					"maxLength": 10
				},
				"shortName": {
					"type": "string",
					"example": "Berlin Basis"
				},
				"description": {
					"$ref": "#/definitions/Description"
				},
				"tariff": {
					"$ref": "#/definitions/Tariff"
				}
			},
			"required": ["id", "shortName", "tariff"]
		},
		"Price": {
			"type": "object",
			"properties": {
				"currency": {
					"type": "string",
					"example": "EUR"
				},
				"amount": {
					"type": "number",
					"format": "double",
					"example": 800
				}
			},
			"required": ["currency", "amount"]
		},
		"Vat": {
			"type": "object",
			"properties": {
				"percent": {
					"type": "number",
					"format": "double",
					"example": 19
				},
				"description": {
					"$ref": "#/definitions/Description"
				}
			},
			"required": ["percent"]
		},
		"Description": {
			"type": "object",
			"properties": {
				"caption": {
					"type": "string"
				},
				"content": {
					"type": "string"
				}
			},
			"required": ["content"]
		},
		"Tariff": {
			"type": "object",
			"properties": {
				"base": {
					"$ref": "#/definitions/BaseCost"
				},
				"consumption": {
					"$ref": "#/definitions/ConsumptionCost"
				}
			}
		},
		"BaseCost": {
			"type": "object",
			"properties": {
				"roof": {
					"type": "number",
					"format": "double",
					"example": 8000
				},
				"recurring": {
					"type": "string",
					"example": "monthly"
				},
				"cost": {
					"$ref": "#/definitions/Price"
				}
			},
			"required": ["cost"]
		},
		"ConsumptionCost": {
			"type": "object",
			"properties": {
				"unit": {
					"type": "string",
					"example": "kWh"
				},
				"cost": {
					"$ref": "#/definitions/Price"
				}
			},
			"required": ["unit", "cost"]
		}
	}
}
