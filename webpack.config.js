
module.exports = {
    entry: "./src/client/bootstrap.tsx",
    output: {
        filename: "./bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }
        ]
    },
    resolve: {
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    }
};
