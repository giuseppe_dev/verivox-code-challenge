import { Component, h } from "preact";
import './App.css';

export default class App extends Component {

    state = { offers: [], consumption: 3000 };

    private setConsumption = (e: any) => {
        if(e.target.value > 0){
            this.setState({consumption: e.target.value} );
        }
    };

    private getOffers = () => {
            fetch(`http://localhost:3001/offers/usage/${this.state.consumption}`)
                .then(result => result.json())
                .then(data => {
                    this.setState({ offers: [...data] });
                })
                .catch(e => {
                    console.log('error: ', e);
                });
    };

    public componentDidMount() {
        this.getOffers();
    }

    public render({}, { offers = [], consumption = 0}) {
        return (
            <form onSubmit={this.getOffers} action="javascript:">
                <label for="consumption">Consumption (kWh/year) </label>
                <input name={'consumption'} type={'number'} value={consumption} onInput={this.setConsumption} min="10" max="100000" step="10" />
                <button type="submit">Get Offer!</button>
                <dl>
                    { offers.map( (offer: any) => (
                        <dd>
                            <span class={'name'}>{offer.productName}</span>
                            <span class={'cost'}>{offer.annualCost} €</span>
                        </dd>
                    )) }
                </dl>
            </form>
        );
    }
}