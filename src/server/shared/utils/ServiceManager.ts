import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

class ServiceManager {
	private service: AxiosInstance;

	constructor(options: AxiosRequestConfig) {
		const service = axios.create({
			...options
		});
		service.interceptors.response.use(this.handleSuccess, this.handleError);
		this.service = service;
	}

	public get(path: string, callback?: any) {
		return this.service
			.get(path)
			.then((response: AxiosResponse) =>
				callback ? callback(response.status, response.data) : response
			);
	}

	private handleError = (error: AxiosError) => Promise.reject(error);

	private handleSuccess(response: AxiosResponse) {
		return response;
	}
}

export default ServiceManager;
