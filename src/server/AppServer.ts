import { Server } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as rateLimit from 'express-rate-limit';
import * as controllers from './controllers';

class AppServer extends Server {
	private readonly logger: Logger;

	private readonly SERVER_STARTED = 'Server started on port: ';
	private readonly SERVER_FALLBACK_MESSAGE = 'Not a supported URL.';

	// This is just one of the many security aids that should be added to the app
	private createAccountLimiter = new rateLimit({
		windowMs: 60 * 60 * 1000, // 1 hour window
		max: 1000, // start blocking after 1000 requests
		message: 'Too many requests from this IP, please try again after an hour'
	});

	constructor() {
		super();

		this.app.use(cors());
		this.logger = new Logger();
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(this.createAccountLimiter);

		this.setupControllers();
	}

	public start(port: number): void {
		this.app.get('*', (req, res) => {
			this.logger.err(new Error(`${req.url} -> ${this.SERVER_FALLBACK_MESSAGE}`));
			res.status(404).json({ error: this.SERVER_FALLBACK_MESSAGE });
		});

		this.app.listen(port, () => {
			this.logger.imp(this.SERVER_STARTED + port);
		});
	}

	private setupControllers(): void {
		const ctlrInstances = [];

		for (const name in controllers) {
			if (controllers.hasOwnProperty(name)) {
				const controller = (controllers as any)[name];
				ctlrInstances.push(new controller());
			}
		}

		super.addControllers(ctlrInstances);
	}
}

export default AppServer;
