import { Product } from '../shared/models/swagger/model/product';

export interface Products extends Array<Product> {}

const baseTariff = {
	id: 'base000',
	shortName: 'basic',
	description: {
		caption: 'Tariff name',
		content: 'Basic electricity'
	},
	tariff: {
		base: {
			recurring: 'monthly',
			cost: {
				currency: 'EUR',
				amount: 5
			}
		},
		consumption: {
			unit: 'kWh',
			cost: {
				currency: 'EUR',
				amount: 0.22
			}
		}
	}
};

const packagedTariff = {
	id: 'packaged000',
	shortName: 'packaged',
	description: {
		caption: 'Tariff name',
		content: 'Packaged tariff'
	},
	tariff: {
		base: {
			roof: 4000,
			recurring: 'yearly',
			cost: {
				currency: 'EUR',
				amount: 800
			}
		},
		consumption: {
			unit: 'kWh',
			cost: {
				currency: 'EUR',
				amount: 0.3
			}
		}
	}
};

const products: Products = [baseTariff, packagedTariff];

export { products, baseTariff, packagedTariff };
