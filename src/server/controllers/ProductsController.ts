import { Controller, Delete, Get, Post, Put } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';
import { Request, Response } from 'express';
import { products } from '../data/products';

@Controller('products')
class ProductsController {
	private readonly logger: Logger;

	private readonly METHOD_NOT_SUPPORTED = 'Method not supported for this endpoint.';

	private readonly EMPTY_RESULTS = 'Sorry, no products found.';

	constructor() {
		this.logger = new Logger();
		this.logger.info('Initialized ProductsController');
	}

	@Get(':id')
	public getOneProduct(req: Request, res: Response): Response {
		// TODO: one function
		const product = products.filter(p => p.id === req.params.id);
		return product && product.length > 0
			? res.status(200).json(product)
			: res.status(404).json({ error: this.EMPTY_RESULTS });
	}

	@Get()
	private getAllProducts(req: Request, res: Response): Response {
		return products && products.length > 0
			? res.status(200).json(products)
			: res.status(404).json({ error: this.EMPTY_RESULTS });
	}

	// Boilerplate but just to show that you can handle unexpected method request with different responses
	@Post()
	private postProducts(req: Request, res: Response): void {
		res.status(404).json({ error: this.METHOD_NOT_SUPPORTED });
	}

	@Put()
	private putProducts(req: Request, res: Response): void {
		res.status(500).json({ error: this.METHOD_NOT_SUPPORTED });
	}

	@Delete()
	private deleteProducts(req: Request, res: Response): void {
		res.status(500).json({ error: this.METHOD_NOT_SUPPORTED });
	}
}

export default ProductsController;
