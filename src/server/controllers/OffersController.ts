import { Controller, Get } from '@overnightjs/core';
import { Logger } from '@overnightjs/logger';
import { Request, Response } from 'express';
import config from '../config';
import { Products } from '../data/products';
import { Tariff } from '../shared/models/swagger/model/tariff';
import ServiceManager from '../shared/utils/ServiceManager';

const productService = new ServiceManager({});

interface Offer {
	productName: string;

	annualCost: number;
}

@Controller('offers')
class OffersController {
	private readonly logger: Logger = new Logger();

	private readonly GENERIC_ERROR = 'An error has occurred.';

	private readonly PARAMETER_NOT_NUMBER = 'Parameter is not a number!';

	private fetchProducts = productService.get(
		config.productBaseUrl,
		(status: number, data: Products) => data
	);

	@Get('usage/:amount')
	public async get({ params }: Request, res: Response) {
		if (!Number(params.amount)) {
			throw new Error(this.PARAMETER_NOT_NUMBER);
		}

		try {
			const products: Products = await this.fetchProducts;
			const suggestedTariffs = this.arrangePerConvenience(params.amount, products);
			return res.status(200).json(suggestedTariffs);
		} catch (error) {
			this.logger.err(error);
			return res.status(500).json({ error: this.GENERIC_ERROR });
		}
	}

	private arrangePerConvenience(consumptionAmount: number, products: Products) {
		const offers: Offer[] = products
			.map(product => {
				const { tariff } = product;
				const { consumption } = tariff;
				const { description, shortName } = product;
				const consumptionCost =
					consumption && consumption.cost.amount ? consumption.cost.amount : 0;

				const baseCost = this.getBaseCost(tariff);
				const chargeableConsumption = this.chargeableConsumption(tariff, consumptionAmount);
				const annualCost = this.calculateOverallCost(
					baseCost,
					consumptionCost,
					chargeableConsumption
				);

				return {
					productName: description ? description.content : shortName,
					annualCost
				};
			})
			.sort((a, b) => (a.annualCost > b.annualCost ? 1 : -1));
		return offers;
	}
	private calculateOverallCost(
		baseCost: number,
		consumptiontCost: number,
		consumptionAmount: number
	) {
		return +(baseCost + consumptionAmount * consumptiontCost).toFixed(2);
	}

	private adaptConsumption(roof: number, consumption: number) {
		return roof < consumption ? consumption - roof : 0;
	}

	// I'm assuming data comes always in monthly or yearly period
	private normalizeBaseCost(cost: number, period: string): number {
		return period === 'monthly' ? cost * 12 : cost;
	}

	private chargeableConsumption({ base, consumption }: Tariff, consumptionAmount: number) {
		if (consumption && consumption.cost.amount) {
			return base && base.roof
				? this.adaptConsumption(base.roof, consumptionAmount)
				: consumptionAmount;
		} else {
			return 0;
		}
	}

	private getBaseCost({ base }: Tariff): number {
		// conservative approach as you could have no base amount at all
		if (base && base.cost) {
			const { cost, recurring } = base;
			// make sure the base tariff is recurring, otherwise assume is payed once (and still include that in the score)
			return recurring ? this.normalizeBaseCost(cost.amount, recurring) : cost.amount;
		} else {
			// No base cost, just return zero to simplify
			return 0;
		}
	}
}

export default OffersController;
