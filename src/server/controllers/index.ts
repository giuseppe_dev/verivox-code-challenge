import Offers from './OffersController';
import Products from './ProductsController';

export { Products, Offers };
