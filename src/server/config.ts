const config = {
	env: process.env.NODE_ENV || 'DEV',
	port: parseFloat(process.env.PORT || '3001'),
	productBaseUrl: 'http://localhost:3001/products'
};

export default config;
